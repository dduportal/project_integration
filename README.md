# Configuration de système d'intégration continue gitlab-ci

 - Installation de Gitlab Runner en suivant la documentation : https://docs.gitlab.com/runner/install/linux-repository.html
   
    curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
    
    sudo apt-get install gitlab-runner
 
 - Configuration de runner spécifique en suivant le document : https://docs.gitlab.com/runner/register/
    
    gitlab-ci coordinator:  https://gitlab.com/
    
    gitlab-ci token: bnm491Q2u5GkrWuxgxw2
    
    gitlab-ci tag : maven
    
    gitlab-ci executor : shell

- Pour voir les paramètres de runner: settings -> CI/CD -> Runner expand


# Creation Pipline


- Projet:
 
API utilitaire en java, le code sous forme d'un projet maven.

- Configuration du fichier :  .gitlab-ci.yml

- 4  Stages : build / test / package / deploy

- Stage : build
  
     script : mvn compile

- Stage : test
  
     script : mvn test
     #condition test automatisé  : when : always

- Stage : package
  
     script : mvn package
    #condition  : allow_failure: true
  
- Stage : deploy
  
     script : mvn verify
    #condition interaction humaine  : when:manual


# Remarques

- temps de travail : 5 à 6 heures sur 4 jours
- J'ai fait beaucoup des essais c'est pourquoi le grand nombre de commits..




